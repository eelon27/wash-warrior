
if(window.location.href.indexOf("parentHome") > -1 ||
window.location.href.indexOf("home") > -1  ){
	var tempE = '';
   sessionStorage.setItem("cartCount", 0);
  $(document).ready(function(){
	  $('.cartItem').removeClass('d-none');
	  loadCart(tempE,'no'); 
    // $(window).scroll(function() {
      // if ($(window).scrollTop() > 500 && $(window).scrollTop() < 2800) {
        // $('#stickyTop').show();
      // }
      // else {
        // $('#stickyTop').hide();
      // }
    // });
	
	 $(".cartIcon a.acart").click(function () {
		$(".cartView").removeClass("cartViewHover");
		$("body").css("overflow", "hidden");
		$(".drawerBackdrop").css("display", "block");
		$(".cartView").addClass("cartShow");

		$(".closeCart,.drawerBackdrop").click(function () {
			$(".cartView").removeClass("cartShow");
			$(".drawerBackdrop").css("display", "none");
			$("body").css("overflow", "auto");
		});
	});
  });

  
  function imgURL(src, size) {
  // remove any current image size then add the new image size
  return src
    .replace(/_(pico|icon|thumb|small|compact|medium|large|grande|original|1024x1024|2048x2048|master)+\./g, '.')
    .replace(/\.jpg|\.png|\.gif|\.jpeg/g, function(match) {
      return '_'+size+match;
    })
  ;
}

	function loadCart(tempE, isShow) {
		debugger;
		var free_item_id = 32423119683655;
		var free_item_dishwasher_id = 39442314723524;
		var free_item_guide_id = 39798618456260;
		$.get(
			"/cart.js",
			function (res) {
				var itemCount = res.item_count;
				if (itemCount > 0) {
					var html = "";
					var total = 0;
					var cartItems = res.items;
					
						if (typeof freeProduct_1 !== "undefined" || typeof freeProduct_2 !== "undefined" || typeof freeProduct_3 !== "undefined") {
						var gift_wraps_in_cart1 = 0;
						var gift_wraps_in_cart2 = 0;
						var gift_wraps_in_cart3 = 0;
						var tags = [];
					
						cartItems.forEach(function(item){
							if (item.id === free_item_id){
								 gift_wraps_in_cart1 = item.quantity;
							}
							if (item.id === free_item_dishwasher_id){
								 gift_wraps_in_cart2 = item.quantity;
							}
							if (item.id === free_item_guide_id){
								 gift_wraps_in_cart3 = item.quantity;
							}
							
								  $.ajax({ 
									url: '/products/' + item.handle + '.js', 
									dataType: 'json',
									async: false, 
									success: function(product){ 
										tags = tags.concat(product.tags);
									} 
								  });  
							
						});
						if (typeof freeProduct_1 !== "undefined" )
							freeProduct_1(tags, gift_wraps_in_cart1, loadCart);
						if (typeof freeProduct_2 !== "undefined")
							freeProduct_2(tags, gift_wraps_in_cart2, loadCart);
						if (typeof freeProduct_3 !== "undefined" )
						 freeProduct_3(tags, gift_wraps_in_cart3, loadCart);
						
					}
					
					$.each(cartItems, function (k, v) {
						if (cartItems[k]["id"] === free_item_id || cartItems[k]["id"] === free_item_dishwasher_id || cartItems[k]["id"] === free_item_guide_id) {
							html +=	'<div class="cartListItem d-flex">\
								 <div><img class=" lazyload" width="100px" data-src="' + imgURL(cartItems[k]["featured_image"]["url"], '100x100') +	'"></div>\
								 <div class="w-100 ml-2">\
								 <h4>' + cartItems[k]["product_title"] + '</h4>\
								 <p>' +	(cartItems[k]["variant_title"] == null || cartItems[k]["variant_title"].length == 0 ? "" : cartItems[k]["variant_title"]) +	'</p>\
								 <div class="d-flex quantity align-items-center justify-content-between f-right">';
						 
							if (cartItems[k]["discounts"].length > 0) {
								html +=	'<h5 style="text-decoration: line-through; color: #717171;">$' +
									(parseInt(cartItems[k]["price"]) * parseInt(cartItems[k]["quantity"])) / 100 + 
									'</h5></div><div class="d-flex py-1 quantity align-items-center justify-content-between">\
									<h5 class="mx-0" style="color: #717171;"><i aria-hidden="true" class="fa fa-tag mirror-x" style="margin-right: 5px"></i>(YKWINNER)</h5><h5>$' +
									parseInt(cartItems[k]["line_price"]) / 100 + "</h5></div></div></div>";
							} else {
								html += "<h5 >$" + (parseInt(cartItems[k]["price"]) * parseInt(cartItems[k]["quantity"])) / 100 + "</h5></div></div></div>";
							}
						}
						
					});

					$.each(cartItems, function (k, v) {
						if (cartItems[k]["id"] !== free_item_id && cartItems[k]["id"] !== free_item_dishwasher_id  && cartItems[k]["id"] !== free_item_guide_id) {
							html +=	'<div class="cartListItem d-flex">\
						 <div><img class=" lazyload" width="100px" data-src="' + imgURL(cartItems[k]["featured_image"]["url"], '100x100') + '"></div>\
						 <div class="w-100 ml-2">\
						 <h4>' + cartItems[k]["product_title"] + "</h4>\
						 <p>" +	(cartItems[k]["variant_title"] == null || cartItems[k]["variant_title"].length == 0 ? "" : cartItems[k]["variant_title"]) +	'</p>\
						 <div class="d-flex quantity align-items-center justify-content-between">\
						<div class="qty-button-container">\
							<div class="quantity">\
								<div class="js-qty">\
									<button type="button" class="js-qty__adjust--minus btn-minus" data-id="' + cartItems[k]["id"] +	'"><span><div class="minusCardButton"></div></span><span class="fallback-text"></span></button>\
									 <input type="text" id="itemCount' + cartItems[k]["id"] + '" value="' +	cartItems[k]["quantity"] + '" placeholder="" aria-label="" aria-describedby="basic-addon1">\
									<button type="button" class="js-qty__adjust--plus btn-plus" data-id="' + cartItems[k]["id"] + '"><span><div class="plusCardButton"></div></span><span class="fallback-text">+</span></button>\
								</div>\
							</div>\
						</div>';
							if (cartItems[k]["discounts"].length > 0) {
								html +=	'<h5 style="text-decoration: line-through; color: #717171;">$' +
									(parseInt(cartItems[k]["price"]) * parseInt(cartItems[k]["quantity"])) / 100 +
									'</h5></div><div class="d-flex py-1 quantity align-items-center justify-content-between">\
									<h5 class="mx-0" style="color: #717171;"><i aria-hidden="true" class="fa fa-tag mirror-x" style="margin-right: 5px"></i>(YKWINNER)</h5><h5>$' +
									parseInt(cartItems[k]["line_price"]) / 100 +
									"</h5></div></div></div>";
							} else {
								html += "<h5 >$" + (parseInt(cartItems[k]["price"]) * parseInt(cartItems[k]["quantity"])) / 100 + "</h5></div></div></div>";
							}
						}
					});

					total = res.total_price / 100;

					$(".cartList").html(html);
					if (isShow == "yes") {
						$(".cartItem a.acart").trigger("click");
					}
					$(".cartTotal h4 i").text("$" + total);
					$(".cartItem a.acart .svg-cart-icon-blue").show();
					$(".cartItem a.acart .svg-cart-icon").hide();
					$(".cartItem a.acart span").text(itemCount);

					sessionStorage.setItem("cartCount", itemCount);
					if ($(".btnShopping").length == 0) {
						if (document.location.pathname.indexOf("/product") >= 0 || document.location.pathname.indexOf("/pages") >= 0) {
							$(".cartTotal").append('<a href="#" class=" btn-block button2  btnShopping">CONTINUE SHOPPING</a>');
						} else {
							$(".cartTotal").append('<a class=" btn-block button2  btnShopping">CONTINUE SHOPPING</a>');
						}
					}
					if ($(".btnCheckout").length == 0) {
						$(".cartTotal").append('<button  type="submit" name="checkout" class=" btn-block button2 btnCheckout">PROCEED TO CHECKOUT</button>');
					}
				} else {
					$(".btnCheckout").remove();
					$(".cartTotal h4 i").text("$0");
					$(".cartItem a.acart .svg-cart-icon-blue").hide();
					$(".cartItem a.acart .svg-cart-icon").show();
					$(".cartItem a.acart span").text(0);
					sessionStorage.setItem("cartCount", 0);
					var html = "<strong>Your cart is empty.</strong>";
					$(".cartList").html(html);
					if ($(".btnShopping").length == 0) {
						if (document.location.pathname.indexOf("/product") >= 0 || document.location.pathname.indexOf("/pages") >= 0) {
							$(".cartTotal").append('<a href="#" class=" btn-block button2  btnShopping">CONTINUE SHOPPING</a>');
						} else {
							$(".cartTotal").append('<a class=" btn-block button2  btnShopping">CONTINUE SHOPPING</a>');
						}
					}
				}
			},
			"json"
		);
	}

	function AddItemToCart(tempE, id) {
		var quantityNum = $(".quantity-num") == null || $(".quantity-num").length == 0 || $(".quantity-num")[0].value == "" ? 1 : $(".quantity-num")[0].value;

		$.ajax({
			type: "POST",
			url: "/cart/add.js",
			dataType: "json",
			data: { id: id, quantity: quantityNum },
			success: function (data) {

				setTimeout(function () {
					loadCart(tempE, "yes");
				}, 250);
				
			},
		});
	}

	function updateCart(tempE, variantId, qty) {
		$.post("/cart/change.js", {
			quantity: qty,
			id: variantId,
			function() {
				setTimeout(function () {
					loadCart(tempE, "yes");
				}, 250);
			},
		});
	}

	$(document).on("click", ".btn-minus", function (e) {
		var id = $(this).data("id");
		var oldQty = $("#itemCount" + id).val();
		var newQty = parseInt(oldQty) - parseInt(1);
		updateCart(e, id, newQty);
	});

	$(document).on("click", ".btn-plus", function (e) {
		var id = $(this).data("id");
		var oldQty = $("#itemCount" + id).val();
		var newQty = parseInt(oldQty) + parseInt(1);
		updateCart(e, id, newQty);
	});

	$(document).on("click", ".btnShopping", function (e) {
		$(".closeCart").trigger("click");
	});

	$(document).on("click", ".btnShoppingContinue", function (e) {
		$(".closeCart").trigger("click");
	});

}

  var buyNow = $('.btn-buy-product');
  buyNow && buyNow.on('click', function (evt) {
	  // var element = document.getElementById("variant-selector");
	  
	    var offsetHeight = $("#scroll-header-text").offset().top;
	   if( $('#scroll-point').css('display')=='none') {
			offsetHeight = offsetHeight - 92;    
		}else{
			offsetHeight = offsetHeight - 92; 
		}
		
$('html, body').animate({
        scrollTop: offsetHeight
      }
                              , 1700, function(){
      }
                             );

  });
